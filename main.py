#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
import time
import jinja2
import os
import json
import logging

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

#key = 'b175a8bc-a5fd-40c9-9ea5-f333903ab48d'

#summoner_name = 'SKT T1 Faker'

class MainHandler(webapp2.RequestHandler):
    def get(self):
        template_values = {
            'tags': "test",
            #'uinfo': userinfo.UserINFO(),
            #'feduri': users.create_login_url(federated_identity=userinfo.PROVIDERMAINURI),
            #'logout': users.create_logout_url('/'),
        }
        template = JINJA_ENVIRONMENT.get_template('/templates/index.html')
        self.response.write(template.render(template_values))
        

app = webapp2.WSGIApplication([
    ('/', MainHandler)
], debug=True)
